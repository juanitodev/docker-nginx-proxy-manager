# Docker Nginx Proxy Manager

Proporcionar a los usuarios una manera fácil de lograr revertir proxy hosts con la terminación SSL con docker y docker-compose.

## Características
- Elegante y Seguro a la Interfaz de Administración basada en la [Tabler](https://tabler.github.io/)
- Crear fácilmente reenvío de dominios, redirecciones, arroyos y 404 hosts sin saber nada acerca de Nginx.
- SSL gratis usando Vamos a Cifrar o proporcionar sus propios certificados SSL.
- Las Listas de acceso y Autenticación HTTP básica para sus anfitriones.
- Configuración avanzada de Nginx disponible para super usuarios.
- Gestión de usuarios, permisos y registro de auditoría.

## Artículos relacionados
- [Nginx Proxy Manager][nginx]

## Dependecias
- Docker
- Docker-compose


# Cómo Implementar

1. Compatibilidad

    Docker Compose version v2.12.0 o superior

2. Ejecute la aplicación con Docker Compose

    La carpeta principal de este repositorio contiene un archivo funcional docker-compose.yaml.  Ejecute la aplicación usándola como se muestra a continuación, sin embargo puedes personalizar el puerto de salida por el que tu quieras:

    ```bash
    $ curl -s https://gitlab.com/juanitodev/docker-nginx-proxy-manager/-/raw/master/run.sh | bash -s 85
    ```

3. Espere a que se inicialice completamente, y visita `http://localhost:85` o `http://host-ip:85` (según corresponda).

    <img src="resources/screenshot/nginx_proxy.png" alt="odoo" width="100%"/>


4. Inicie sesión en la interfaz de usuario de Admin

    De Usuario De Administrador Predeterminada:

    ```console
    Email:    admin@example.com
    Password: changeme
    ```
    Inmediatamente después de iniciar sesión con este usuario predeterminado, se le pedirá que modifique sus datos y cambie su contraseña.


[nginx]: https://nginxproxymanager.com/ "Nginx Proxy Manager"