#!/bin/bash
# Quick Setup

PROJECT_NAME=proxy
PORT=$1

# Project's name
git clone https://gitlab.com/juanitodev/docker-nginx-proxy-manager $PROJECT_NAME
cd $PROJECT_NAME
rm -rf .git
rm -rf resources
rm README.md

# config
sed -i 's/85/'$PORT'/g' docker-compose.yaml

# Running nginx
docker compose up -d
